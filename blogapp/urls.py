from django.urls import path
from . import views

urlpatterns = [
    # Website Frontend
    path('', views.display_blog, name='index'),
    path('author/<name>', views.getauthors, name="author"),
    path('details/<int:id>', views.singlepost, name='single'),
    path('category/<name>', views.get_by_category, name="category"),
    path('contact', views.contact, name="contact"),

    # user credential
    path('login', views.get_login, name="login"),
    path('logout', views.get_logout, name="logout"),
    path('register', views.get_register, name="register"),

    # dashboard, profile
    path('dashboard', views.dashboard, name="dashboard"),
    path('profile', views.profile, name="profile"),

    # Article Section
    path('create-article', views.create_article, name="create_article"),
    path('article_list', views.article_list, name="article_list"),
    path('article_update/<int:id>', views.article_update, name="article_update"),
    path('article_delete/<int:id>', views.article_delete, name="article_delete"),
]
