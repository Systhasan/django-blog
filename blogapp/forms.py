from django import forms
from .models import Article
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class ArticleForms(forms.ModelForm):
    title = forms.CharField(
        # required=True,
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Article Title'
            })
    )

    body = forms.CharField(
        help_text='Write your article here.',
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'placeholder': 'Write your article here.'
            })
    )

    # A hidden input for internal use
    # source = forms.CharField(
    #     max_length=50,
    #     widget=forms.HiddenInput()
    # )

    def clean(self):
        cleaned_data = super(ArticleForms, self).clean()
        title = cleaned_data.get('title')
        body = cleaned_data.get('body')
        if not title and not body:
            raise forms.ValidationError('You have to write something!')

    class Meta:
        model = Article
        fields = [
            'title',
            'body',
            'image',
            'category'
        ]


'''
@name    RegisterUserForms
@params  Object {UserCreationForm}
@desc    User registration forms, 
         Its uses for register an user.
'''


class RegisterUserForms(UserCreationForm):

    first_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control border-0 shadow form-control-lg',
                'placeholder': 'First Name'
            })
    )

    last_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control border-0 shadow form-control-lg',
                'placeholder': 'Last Name'
            }
        )
    )

    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control border-0 shadow form-control-lg',
                'placeholder': 'Username'
            }
        )
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control border-0 shadow form-control-lg',
                'placeholder': 'Write Email address'
            }
        )
    )

    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control border-0 shadow form-control-lg text-violet',
                'placeholder': 'Write Password'
            }
        )
    )

    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control border-0 shadow form-control-lg text-violet',
                'placeholder': 'Write Password'
            }
        )
    )

    # def clean(self):
    #     cleaned_data = super(RegisterUserForms, self).clean()
    #     password1 = cleaned_data.get('password1')
    #     password2 = cleaned_data.get('password2')
    #     if password1 != password2:
    #         raise forms.ValidationError("Password don't match.")

    class Meta:
        model = User
        fields = [
            'last_name',
            'first_name',
            'email',
            'username',
            'password1',
            'password2'
        ]
