from django.contrib import admin
from .models import Article, Author, Category
# Register your models here.


class ArticleModel(admin.ModelAdmin):
    list_display = ["__str__", "posted_on"]
    search_fields = ["__str__", "details"]
    list_filter = ['posted_on', 'category']
    list_per_page = 12

    class Meta:
        Model = Article


admin.site.register(Article, ArticleModel)


class AuthorModel(admin.ModelAdmin):
    list_display = ["__str__"]
    search_fields = ["__str__"]
    list_per_page = 12

    class Meta:
        Model = Author


admin.site.register(Author, AuthorModel)


class CategoryModel(admin.ModelAdmin):
    list_display = ["__str__"]
    search_fields = ["__str__"]
    list_per_page = 12

    class Meta:
        Model = Category


admin.site.register(Category, CategoryModel)
