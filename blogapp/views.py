from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from .models import Article, Category, User, Author
from django.contrib.auth import authenticate, login, logout
from djangoblog.services import get_paginator
from django.contrib import messages
from .messages import *

# Import Form classes
from .forms import ArticleForms, RegisterUserForms

# Create your views here.


"""
@name
@param 
@return 
@desc 
"""


def display_blog(request):
    # get all article.
    post = Article.objects.all().order_by('-id')

    # get latest article.
    latest_post = Article.objects.order_by('-id')[:2]

    # get Trend article.
    trend_id = Category.objects.filter(name="Trend")
    trend_post = Article.objects.filter(category=3)

    # dictionary for response data.
    context = {
        'post': get_paginator(request, post, per_page=5),
        'latest': latest_post,
        "trend": trend_post
    }

    # return
    return render(request, 'index.html', context)


"""
@name
@param 
@return 
@desc 
"""


def getauthors(request, name):
    post_author = get_object_or_404(User, username=name)
    auth = get_object_or_404(Author, name=post_author.id)
    post = Article.objects.filter(article_author=auth.id)
    context = {
        'post': post,
        'auth': auth
    }
    return render(request, 'author.html', context)


"""
@name
@param 
@return 
@desc 
"""


def singlepost(request, id):
    post = get_object_or_404(Article, pk=id)
    context = {
        'post': post,
    }
    return render(request, 'single.html', context)


"""
@name
@param 
@return 
@desc 
"""


def get_by_category(request, name):
    cat = get_object_or_404(Category, name=name)
    post = Article.objects.filter(category_id=cat.id)
    context = {
        'post': get_paginator(request, post, per_page=4),
        'title': name
    }
    return render(request, 'category.html', context)


"""
@name
@param 
@return 
@desc 
"""


def get_login(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == "POST":
            user = request.POST.get("username")
            password = request.POST.get("password")
            auth = authenticate(request, username=user, password=password)
            if auth is not None:
                login(request, auth)
                return redirect('dashboard')
            else:
                messages.add_message(request, messages.ERROR, LOGIN_ERROR)
    return render(request, 'client/user/login_client.html')


"""
@name   get_logout
@param  {object} request
@return redirect
@desc   Get logout from user account. 
"""


def get_logout(request):
    # clear user data.
    logout(request)
    messages.success(request, LOGOUT_SUCCESS)
    # return redirect
    return redirect('login')


"""
@name   contact
@param  {object} request
@return redirect
@desc   render to the contact page.
"""


def contact(request):
    return render(request, 'contact.html')


"""
@name   create_article
@param  {object} request
@return redirect
@desc   add a new article.
"""


def create_article(request):
    if request.user.is_authenticated:
        form = ArticleForms(request.POST or None, request.FILES or None)
        u = get_object_or_404(Author, name=request.user.id)
        if form.is_valid():
            data = form.save(commit=False)
            data.article_author = u
            data.save()
            messages.success(request, ARTICLE_ADD_SUCCESS)
            return redirect('article_list')
    else:
        messages.error(request, AUTH_ERROR_MESSAGE)
        return redirect('login')

    context = {
        "form": form
    }

    # return render(request, 'create-article.html', context)
    return render(request, 'client/article/add_article.html', context)


"""
@name   dashboard
@param  {object} request
@return redirect
@desc   redirect to the client Dashboard. 
"""


def dashboard(request):
    context = {
        'client': 1
    }
    return render(request, 'client/dashboard.html', context)


"""
@name   profile
@param  {object} request
@return redirect
@desc   get user profile data and article.
"""


def profile(request):
    if request.user.is_authenticated:
        # get user posted data.
        post = Article.objects.filter(article_author=request.user.id)
        user = get_object_or_404(Author, name=request.user.id)
        context = {
            'post': post,
            'user': user
        }
        return render(request, 'client/user/profile.html', context)
    else:
        return redirect('login')


"""
@name   article_list
@param  {object} request
@return redirect
@desc   get all article list.
"""


def article_list(request):
    post = Article.objects.filter(article_author=request.user.id)
    context = {
        'post': post
    }
    return render(request, 'client/article/article_list.html', context)


"""
@name   article_update
@param  {object} request
@return {redirect} 
@desc   update article using its given id.
"""


def article_update(request, id):
    if request.user.is_authenticated:
        u = get_object_or_404(Author, name=request.user.id)
        post = get_object_or_404(Article, id=id)
        form = ArticleForms(request.POST or None,
                            request.FILES or None, instance=post)
        if form.is_valid():
            data = form.save(commit=False)
            data.article_author = u
            data.save()
            messages.add_message(request, messages.SUCCESS, ARTICLE_UPDATE)
            return redirect('article_list')
    else:
        return redirect('login')

    context = {
        "form": form
    }
    return render(request, 'client/article/add_article.html', context)


"""
@name   article_delete
@param  {object} request
@return {redirect} 
@desc   delete an article using its given id.
"""


def article_delete(request, id):
    if request.user.is_authenticated:
        post = get_object_or_404(Article, id=id)
        post.delete()
        return redirect('article_list')
    else:
        return render(request, 'login')


"""
@name   get_register
@param  {object} request
@return {redirect} 
@desc   Register an user.
"""


def get_register(request):
    form = RegisterUserForms(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            data = form.save(commit=False)
            data.save()
            messages.success(request, REGISTER_USER_SUCCESS)
            return redirect('login')
        else:
            messages.error(request, form.errors)
            return redirect('register')
    context = {
        "form": form
    }
    return render(request, 'client/user/register_client.html', context)
