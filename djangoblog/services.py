from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

'''
@name get_paginator
@param request, 
        {string}db table name
@return 
@desc  get row pagination data. 
'''


def get_paginator(request, table, per_page):
    paginator = Paginator(table, per_page)
    page = request.GET.get('page')
    total_article = paginator.get_page(page)
    return total_article
